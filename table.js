/**
* Создание изменяемой ячейки
*
* @param {number} j Номер столбца
* @param {number} i Номер строки
* @param {number} posx Позиция по оси X
* @param {number} posy Позиция по оси Y
*/

function createCell(j, i, posx, posy)
{
    var color = Raphael.getRGB("rgb(0,0,0)");
    var fakeColor = Raphael.getRGB("rgb(200,80,30)");
    r.rect(posx, posy, sizeCell, sizeCell).data("i", i).data("j", j).attr({
        stroke: color,
        fill: color,
        "fill-opacity": .0,
        "stroke-width": 0.5
    }).mousedown(

        /**
        * События мыши
        *
        * @param {number} e Номер нажатой кнопки мыши 
        */
        
        function(e) 
        {
            if(e.which==1)
            {
                if (isActive[this.data("j")][this.data("i")]) this.animate({
                    "fill-opacity": .0
                }, 300);
                else this.animate({
                    fill: color,
                    "fill-opacity": .7
                }, 300);
                isActive[this.data("j")][this.data("i")] = !isActive[this.data("j")][this.data("i")];
            }
            else if(e.which==3)
            {
                if (isActive[this.data("j")][this.data("i")]) 
                isActive[this.data("j")][this.data("i")] = false;
                this.animate({
                    "fill-opacity": .3,
                    fill: fakeColor
                }, 300);
            }

            checkColumnHorizontal(this.data("j"));
            checkColumnVertical(this.data("i"));
            checkAll();
        }
    );
}

/**
* Создание статической ячейки
*
* @param {number} posx Позиция по оси X
* @param {number} posy Позиция по оси Y
*/

function createStaticCell(posx, posy)
{
    var color = Raphael.getRGB("rgb(0,0,0)");
    var succesColor = Raphael.getRGB("rgb(100,250,100)");;

    r.rect(posx, posy, sizeCell, sizeCell).attr({
        stroke: color,
        fill: color,
        "fill-opacity": .2,
        "stroke-width": 0.5
    }).mousedown(

        function(e) 
        {
            if(e.which==1)
            {
                this.animate({
                    fill: succesColor,               
                }, 300);
            }
            if(e.which==3)
            {
                this.animate({
                    fill: color,               
                }, 300);
            }
        }
    );
}