/**
* Конструктор уровня по заданному массиву
*
* @param {array} arrH горизонтальный массив
* @param {array} arrV вертикальный массив
*/

function Build (arrH, arrV) 
{
    CreateText(arrH, arrV);
    CreateMainCells(arrH, arrV);
    CreateStaticCells(arrH, arrV);
    ShowLevelText();
    ShowLine();
    currentHorizontal = new Array(cellCountX);
    currentVertical = new Array(cellCountY);
}

/**
* Показать название уровня
*/

function ShowLevelText() 
{
    r.text(10 + staticCellCount*sizeCell/2, 10 + staticCellCount*sizeCell/2 - 10, "Level " + levelName).attr({font: "18px Helvetica"});
}

/**
* Показать разделительные линии
*/

function ShowLine () 
{
    for (var i = 0; i <= (cellCountX/5) + 1; i++) 
    {
        r.path("M "+ (10 + i*sizeCell*5) + " 10 V" + (10+(staticCellCount+cellCountY)*sizeCell)).attr({"stroke-width":2.5});
    };
    for (var i = 0; i <= (cellCountY/5) + 1; i++) 
    {
        r.path("M 10 "+ (10 + i*sizeCell*5) + " H" + (10+(staticCellCount+cellCountX)*sizeCell)).attr({"stroke-width":2.5});
    };
}

/**
* Отображение цифровых надпписей
*
* @param {array} arrH горизонтальный массив
* @param {array} arrV вертикальный массив
*/

function CreateText(arrH, arrV)
{
    var halfSize = sizeCell/2;
    for(var i = 0; i < arrV.length; i++)
    {
        for(var j = 0; j < arrV[i].length; j++)
        {
            r.text(10 + (staticCellCount - arrV[i].length)*sizeCell + sizeCell*j + halfSize, 
                   10 + sizeCell*staticCellCount + sizeCell*i + halfSize, 
                    arrV[i][j]).attr({font: "14px Helvetica"});
        }
    }
    for(var i = 0; i < arrH.length; i++)
    {
        for(var j = 0; j < arrH[i].length; j++)
        {
            r.text(10 + sizeCell*staticCellCount + sizeCell*i + halfSize,
                   10 + (staticCellCount - arrH[i].length)*sizeCell + sizeCell*j + halfSize,
                   arrH[i][j]).attr({font: "14px Helvetica"});
        }
    }
}

/**
* Создание изменяемых ячеек
*
* @param {array} arrH горизонтальный массив
* @param {array} arrV вертикальный массив
*/

function CreateMainCells(arrH, arrV)
{
    isActive = new Array(cellCountY)
    for (var j = 0; j < cellCountY; j++) 
    {
        isActive[j] = new Array(cellCountX);
        for (var i = 0; i < cellCountX; i++) 
        {
            isActive[j][i] = false;
            createCell(j,i,10 + sizeCell*staticCellCount + sizeCell * i,10 + sizeCell*staticCellCount + sizeCell * j);
        }
    }
}

/**
* Создание статических ячеек
*
* @param {array} arrH горизонтальный массив
* @param {array} arrV вертикальный массив
*/

function CreateStaticCells(arrH, arrV)
{
    for (var j = 0; j < cellCountY; j++) 
    {
        for (var i = 0; i < staticCellCount; i++)
        {
            createStaticCell(10 + sizeCell*i, 10 + sizeCell*staticCellCount + sizeCell*j);
        } 
    }
    for (var j = 0; j < cellCountX; j++) 
    {
        for (var i = 0; i < staticCellCount; i++)
        {
            createStaticCell(10 + sizeCell*staticCellCount + sizeCell * j, 10 + sizeCell*i);
        } 
    }
}