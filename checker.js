/**
* Проверяет горизонтальный столбец и записывает состояние закрашеных ячеек в массив
*
* @param {number} column
*/

function checkColumnHorizontal (column) 
{
    var arr = new Array();
    var curEl = 0;
    var curK = 0;
    for (var i = 0; i < cellCountX; i++) 
    {
        if(isActive[column][i])
        {   
            curK++;  
            if(i == cellCountX-1)
            {
                arr[curEl]=curK;
                curK = 0;
                curEl++;
            } 
        }
        else
        {
            if(curK>0)
            {
                arr[curEl]=curK;
                curK = 0;
                curEl++;
            }
        }
    };
    currentHorizontal[column] = arr;
}

/**
* Проверяет вертикальный столбец и записывает состояние закрашеных ячеек в массив
*
* @param {number} column
*/

function checkColumnVertical (column) 
{
    var arr = new Array();
    var curEl = 0;
    var curK = 0;
    for (var i = 0; i < cellCountY; i++) 
    {
        if(isActive[i][column])
        {
            curK++;
            if(i == cellCountX-1)
            {
                arr[curEl]=curK;
                curK = 0;
                curEl++;
            } 
        }
        else
        {
            if(curK>0)
            { 
                arr[curEl]=curK;
                curK = 0;
                curEl++;
            } 
        }   
    };
    currentVertical[column] = arr;
}

/**
* Сравнивает текущий массив заполненых ячеек с массивом условия задачи
*/

function checkAll()
{
    var ch = serialize(currentHorizontal);
    var av = serialize(arrayVertical);
    var cv = serialize(currentVertical);
    var ah = serialize(arrayHorizontal);
    if((ch==av)&&(cv==ah))
    {
        bootstrap_alert = function() {}
        bootstrap_alert.info = function(message) 
        {
            $('#alert_placeholder').html('<div class="alert alert-success" style="width: 350px; "><a class="close" data-dismiss="alert">x</a><span>'+message+'</span></div>')
        }
        bootstrap_alert.info('<b>Congratulations!</b> Level is passed');
    }
}